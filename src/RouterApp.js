import React from "react";
import { useSelector } from "react-redux";
import { accessTokenSelector } from "./redux/selectors";

import Login from "./pages/Login";

function RouterApp({ children, ...rest }) {
  const access = useSelector(() => accessTokenSelector());

  if (!access) {
    console.log("Login required. Redirecting...");
    return <Login />;
  }

  return <>{children}</>;
}

export default RouterApp;
