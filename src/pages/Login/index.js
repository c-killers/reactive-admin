import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Card,
  CardContent,
  CardActions,
  Container,
  FormControl,
  Input,
  InputLabel,
  Typography,
  Button
} from "@material-ui/core";

import { adminLogin, fetchUserData } from "../../redux/actions";
import { navigate } from "@reach/router";

import {
  userAuthSelector,
  userSelector,
  isLoggedInSelector
} from "../../redux/selectors";
import styles from "./styles";

export default ({ path }) => {
  const classes = styles();

  const [username, setUsername] = useState("");
  const [password, setPasswd] = useState("");
  const [attempts, increaseAttempts] = useState(0);

  const dispatch = useDispatch();

  const authData = useSelector(state => userAuthSelector(state));
  const user = useSelector(state => userSelector(state));
  const loggedIn = useSelector(state => isLoggedInSelector(state));

  const handleOnChangeUsername = event => {
    setUsername(event.target.value);
  };

  const handleOnChangePasswd = event => {
    setPasswd(event.target.value);
  };

  const handleLoginClick = () => {
    dispatch(adminLogin({ username, password }));
    console.log("adminLogin action dispatched");
    increaseAttempts(attempts + 1);
  };

  useEffect(() => {
    if (loggedIn && !user) {
      dispatch(fetchUserData({ authData }));
      console.log("useEffect fetching user...");
    }
    if (user) {
      console.log(
        `User is logged in after ${attempts} attempts. Redirecting home...`
      );
      navigate("/");
    }
  }, [authData, loggedIn, user, attempts, dispatch]);

  return (
    <Container className={classes.container}>
      <Card className={classes.cardContainer}>
        <CardContent className={classes.mainContent}>
          <Typography variant="h5">Log in</Typography>
          <FormControl margin="dense">
            <InputLabel htmlFor="username">Username</InputLabel>
            <Input
              id="username"
              autoFocus={true}
              onChange={handleOnChangeUsername}
            />
          </FormControl>
          <FormControl margin="dense">
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              id="password"
              type="password"
              autoComplete="false"
              onChange={handleOnChangePasswd}
            />
          </FormControl>
        </CardContent>
        <CardActions className={classes.actions}>
          <Button
            fullWidth={true}
            variant="contained"
            color="primary"
            onClick={handleLoginClick}
          >
            Log in
          </Button>
        </CardActions>
      </Card>
    </Container>
  );
};
