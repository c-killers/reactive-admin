import { makeStyles } from "@material-ui/styles";

const centeredStyleObj = {
  display: "flex",
  justifyContent: "center"
};

export default makeStyles({
  container: {
    ...centeredStyleObj
  },
  mainContent: {
    flexDirection: "column",
    maxWidth: "20rem",
    padding: "20px 32px 15px",
    ...centeredStyleObj
  },
  actions: {
    padding: "0px 32px 25px"
  }
});
