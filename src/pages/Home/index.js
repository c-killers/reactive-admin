import React, { useEffect } from "react";

import { Grid, Typography } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";

import { userSelector } from "redux/selectors";
import { fetchUserData } from "redux/actions";

import Header from "components/Header";
import ModelCard from "components/ModelCard";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(5)
  },
}));

export default () => {
  const dispatch = useDispatch();
  const user = useSelector(state => userSelector(state));
  const models = [
    {name: 'Projects', icon: 'folder'},
    {name: 'Pipelines', icon: 'account_tree'},
    {name: 'Stages', icon: 'menu'},
    {name: 'Events', icon: 'event_note'},
    {name: 'Pipeline Messages', icon: 'inbox'}
  ];
  const classes = useStyles();

  useEffect(() => {
    if (!user) {
      dispatch(fetchUserData({}));
    }
  }, [user, dispatch]);

  return (
    <>
      <Header>
        <Typography>Welcome, {user?.username}</Typography>
      </Header>
      <Grid container className={classes.root} spacing={5}>
        { models.map((model, index) => <ModelCard key={index} modelName={model.name} icon={model.icon} />) }
      </Grid>
    </>
  );
};
