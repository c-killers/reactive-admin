import { get } from "lodash";
import store from "store";

export const userAuthSelector = state => get(state, "auth.authData");
export const userSelector = state => get(state, "auth.user");
export const accessTokenSelector = () => store.get("accessToken");
export const isLoggedInSelector = state => get(state, "auth.loggedIn");
export const fetchUserInProgressSelector = state =>
  get(state, "auth.fetchUserInProgress");
