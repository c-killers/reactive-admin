import {
  ADMIN_LOGIN_START,
  ADMIN_LOGIN_FETCH_USER_DATA_START,
  ADMIN_SIGNOUT_START
} from "../../consts/actionTypes";

export const adminLogin = payload => ({
  type: ADMIN_LOGIN_START,
  payload
});

export const fetchUserData = payload => ({
  type: ADMIN_LOGIN_FETCH_USER_DATA_START,
  payload
});

export const signout = payload => ({
  type: ADMIN_SIGNOUT_START,
  payload
});
