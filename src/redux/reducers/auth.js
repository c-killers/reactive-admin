import {
  ADMIN_LOGIN_START,
  ADMIN_LOGIN_ERROR,
  ADMIN_LOGIN_COMPLETE,
  ADMIN_LOGIN_FETCH_USER_DATA_START,
  ADMIN_LOGIN_FETCH_USER_DATA_ERROR,
  ADMIN_LOGIN_FETCH_USER_DATA_COMPLETE,
  ADMIN_SIGNOUT_START,
  ADMIN_SIGNOUT_ERROR,
  ADMIN_SIGNOUT_COMPLETE
} from "../../consts/actionTypes";

export default (state, action) => {
  switch (action.type) {
    case ADMIN_LOGIN_START:
      console.log("action admin login start", action);
      return { ...state, loggedIn: false };
    case ADMIN_LOGIN_ERROR:
      console.log("action admin login error", action);
      return { ...state, loggedIn: false, authData: null };
    case ADMIN_LOGIN_COMPLETE:
      console.log("action admin login complete", action);
      return { ...state, loggedIn: true, authData: action.response.data };

    case ADMIN_LOGIN_FETCH_USER_DATA_START:
      console.log("action fetch user start", action);
      return { ...state, fetchUserInProgress: true };
    case ADMIN_LOGIN_FETCH_USER_DATA_ERROR:
      console.log("action fetch user error", action);
      return { ...state, fetchUserInProgress: false, user: null };
    case ADMIN_LOGIN_FETCH_USER_DATA_COMPLETE:
      console.log("action fetch user complete", action);
      return {
        ...state,
        fetchUserInProgress: false,
        user: action.response.data
      };

    case ADMIN_SIGNOUT_START:
      console.log("action admin signout start", action);
      return { ...state };
    case ADMIN_SIGNOUT_ERROR:
      console.log("action admin signout error", action);
      return { ...state, loggedIn: false, action };
    case ADMIN_SIGNOUT_COMPLETE:
      console.log("action admin signout complete", action);
      return { ...action, loggedIn: false };
    default:
      return { ...state };
  }
};
