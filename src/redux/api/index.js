import axios from "axios";

const baseURL = "http://localhost:8000/api/";

export const apiCall = (url, data, headers, method) =>
  axios({
    method,
    url: baseURL + url,
    data,
    headers
  });
