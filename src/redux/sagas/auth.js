import { call, put, takeLatest } from "redux-saga/effects";
import store from "store";
import { useDispatch, useSelector } from "react-redux";
import { isEmpty } from "lodash";

import {
  ADMIN_LOGIN_START,
  ADMIN_LOGIN_ERROR,
  ADMIN_LOGIN_COMPLETE,
  ADMIN_LOGIN_FETCH_USER_DATA_START,
  ADMIN_LOGIN_FETCH_USER_DATA_ERROR,
  ADMIN_LOGIN_FETCH_USER_DATA_COMPLETE,
  ADMIN_SIGNOUT_START,
  ADMIN_SIGNOUT_ERROR,
  ADMIN_SIGNOUT_COMPLETE
} from "../../consts/actionTypes";
import { apiCall } from "../api";

export function* loginStart({ payload }) {
  try {
    const response = yield call(
      apiCall,
      "token",
      payload,
      { "Content-Type": "application/json" },
      "POST"
    );
    store.set("accessToken", response.data.access);
    yield put({ type: ADMIN_LOGIN_COMPLETE, response });
  } catch (error) {
    yield put({ type: ADMIN_LOGIN_ERROR, error });
  }
}

export function* fetchUser({ payload }) {
  const access = isEmpty(payload)
    ? store.get("accessToken")
    : payload.authData.access;
  const headers = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${access}`
  };
  try {
    const response = yield call(apiCall, "me", null, headers);
    yield put({ type: ADMIN_LOGIN_FETCH_USER_DATA_COMPLETE, response });
  } catch (error) {
    store.remove("accessToken");
    yield put({ type: ADMIN_LOGIN_FETCH_USER_DATA_ERROR, error });
  }
}

export function* userSignOut({ payload }) {
  if (payload.fullCleanUp) {
    store.remove("accessToken");
    console.log('accessToken removed');
    const response = {authData: null, user: null};
    yield put({ type: ADMIN_SIGNOUT_COMPLETE, ...response });
  }
}

export default function* auth() {
  yield takeLatest(ADMIN_LOGIN_START, loginStart);
  yield takeLatest(ADMIN_LOGIN_FETCH_USER_DATA_START, fetchUser);
  yield takeLatest(ADMIN_SIGNOUT_START, userSignOut);
}
