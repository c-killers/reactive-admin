import React from "react";
import { Provider } from "react-redux";
import { PropTypes } from "prop-types";
import { Router } from "@reach/router";

import Login from "./pages/Login";
import Home from "./pages/Home";
import RouterApp from "./RouterApp";

const App = ({ store }) => {
  return (
    <Provider store={store}>
      <Router>
        <Login path="/login" />
        <RouterApp path="/">
          <Home path="/" />
        </RouterApp>
      </Router>
    </Provider>
  );
};

App.propTypes = {
  store: PropTypes.object.isRequired
};

export default App;
