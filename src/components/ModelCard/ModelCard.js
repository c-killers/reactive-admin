import React from "react";
import { Grid, Card, CardContent, CardActionArea, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles(theme => ({
  card: {
    display: "flex",
    flexDirection: "column",
    height: 150,
    width: 230,
  }
}));

const ModelCard = ({ modelName, icon }) => {
  const classes = useStyles();
  const handleClick = () => {
    console.log("Handle Click");
  }
  return (
    <Grid item>
      <Card>
        <CardActionArea className={classes.card} onClick={handleClick}>
          <Icon style={{fontSize: 90}}>{icon}</Icon>
          <CardContent>
            <Typography variant="h6">{modelName}</Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
}

export default ModelCard;
