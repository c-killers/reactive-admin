import React from "react";

import { useDispatch } from "react-redux";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { Button, Typography } from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles";

import { signout } from "redux/actions";

const useStyles = makeStyles(theme => ({
  title: {
    flexGrow: 1
  },
  root: {
    padding: theme.spacing(1)
  }
}));

const Header = ({ children }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const handleSignOutClick = event => {
    dispatch(signout({fullCleanUp: true}));
  }

  return (
    <AppBar position="static" className={classes.root}>
      <Toolbar>
        <Typography className={classes.title}>Admin Ckiller App</Typography>
        {children}
        <Button color="inherit" value="signout" onClick={handleSignOutClick}>Sign out</Button>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
